package view;

import controller.Parser;

public class View {
    public static void main(String[] args) {
        Parser parser = new Parser();
        parser.run();
    }
}
