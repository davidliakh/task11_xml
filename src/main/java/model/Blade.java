package model;

public class Blade {

  private int length;
  private int width;
  private String material;
  private boolean isBloodstream;

  public Blade() {
  }

  Blade(int length, int width, String material,
      boolean isBloodstream) {
    this.length = length;
    this.width = width;
    this.material = material;
    this.isBloodstream = isBloodstream;
  }

  public String getMaterial() {
    return material;
  }

  public void setMaterial(String material) {
    this.material = material;
  }

  public boolean isBloodstream() {
    return isBloodstream;
  }

  public void setBloodstream(boolean bloodstream) {
    isBloodstream = bloodstream;
  }

  public int getLength() {
    return length;
  }

  public void setLength(int length) {
    this.length = length;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }
}
